<?php 
/**
 * @file - link exporer module file, helps to explore links inside drupal nodes
 * @author - mehul.parmar@mphasis.com
 * @created - 06 March 2015
 */

 /**
  * Implementing hook_menu()
  *   Creates menus required for the module
  * @return array $items
  *   Returns array of menus intoduced by module.
  */
 function linkexplorer_menu() {
 	$items["admin/config/development/linkexplorer"] = array(
 	  "title" => t("Link explorer options"),
 	  "page callback" => "link_explorer_options",
 	  "access arguments" => array("use link explorer"),
 	);
 	$items["admin/config/development/linkexplorer/settings"] = array(
 	  "title" => t("Link explorer settings"),
 	  "page callback" => 'drupal_get_form',
      "page arguments" => array('link_explorer_settings'),
 	  "access arguments" => array("admin link explorer"),
 	);
 	$items["admin/config/development/linkexplorer/showlinks"] = array(
 	  "title" => t("Export data to excel sheet"),
 	  "page callback" => "link_explorer_form",
 	  "access arguments" => array("use link explorer"), 
 	  "file" => 'PHPExcel.php', 
 	  "file path" => drupal_get_path('module', 'linkexplorer'),
 	);
 	$items["admin/config/development/linkexplorer/removesuffix"] = array(
 	  "title" => t("Remove string using regular expression"),
 	  "page callback" => "link_explorer_remove_html_suffix",
 	  "access arguments" => array("use link explorer"),
 	);
 	$items["admin/config/development/linkexplorer/flushvariables"] = array(
 	  "title" => t("Flush all variables"),
 	  "page callback" => "flush_batch_variables",
 	  "access arguments" => array("admin link explorer"),
 	);
 	$items["admin/config/development/linkexplorer/removeusingxls"] = array(
 	  "title" => t("Remove using excel sheet options"),
 	  "page callback" => "link_explorer_remove_using_xls",
 	  "access arguments" => array("use link explorer"),
 	  "file" => 'PHPExcel.php',
 	  "file path" => drupal_get_path('module', 'linkexplorer'),
 	);
 	return $items;
 }
 /**
  * Implementing link_explorer_remove_html_suffix()
  *   Removes all .html or .htm suffix from all url of all nodes
  */
 function link_explorer_remove_html_suffix() {
 	//$regular_express = "/(.htm|.html)$/";
 	$regular_express = variable_get("link_replace_regular_expression", "");
 	$links = get_links_in_node();
 	foreach ($links as $link) {
 	  $node_and_links[$link['node']][] = preg_replace($regular_express, "", $link['link']);
 	}
 	return "<pre>". print_r($node_and_links, TRUE)."</pre>";
 }
 function link_explorer_settings() {
 	$form = array();
 	$form['link_match_regular_expression'] = array(
 	  "#title" => t("Regular expression to find strings"),
 	  "#type" => "textfield",
 	  "#default_value" => variable_get("link_match_regular_expression", '@href=[\'"]?([^\'" >]+)@'),
 	  "#description" => t("Provide the regular expressions to match the strings."),
 	);
 	$form['link_replace_regular_expression'] = array(
 	  "#title" => t("Regular expression to replace strings"),
 	  "#type" => "textfield",
 	  "#default_value" => variable_get("link_replace_regular_expression", "/(.htm|.html)$/"),
 	  "#description" => t("Provide the regular expressions to replace the string in content."),
 	);
 	$form['link_export_xls_path'] = array(
 	  "#title" => t("Path to save the excel sheet."),
 	  "#type" => "textfield",
 	  "#default_value" => variable_get("link_export_xls_path", "sites/default/files"),
 	  "#description" => t("Pleae provide path to save the excel sheet.
 	    Please do not add any trailing or starting slash."),
 	);
 	$form['link_replace_batch_size'] = array(
 	  "#title" => t("Batch size for replacement."),
 	  "#type" => "textfield",
 	  "#default_value" => variable_get("link_replace_batch_size", 0),
 	  "#description" => t("Pleae provide batch size if you have large number of nodes. Set 0 for no batching."),
 	);
 	return system_settings_form($form);
 }
 /**
  * Implementing link_explorer_options()
  *   Shows available options of module
  * @return string
  *   HTML list of available options
  */
 function link_explorer_options() {
 	$op = array();
 	$op[] = l("Show me all the links in node", 
 	  url("admin/config/development/linkexplorer/showlinks", array("absolute" => TRUE)));
 	$op[] = l("I want to replace all matching strings from links", 
 	  url("admin/config/development/linkexplorer/removesuffix", array("absolute" => TRUE)));
 	$op[] = l("I want to replace links with reference to excel sheet I have provided", 
 	  url("admin/config/development/linkexplorer/showlinks", array("absolute" => TRUE)));
 	if (user_access("admin link explorer")) {
 	  $op[] = l("I want to change the settings", 
 	    url("admin/config/development/linkexplorer/settings", array("absolute" => TRUE)));
 	}
 	return theme("item_list", array("items" => $op));
 }
 /**
  * Implmenting hook_permission()
  *   Creates required permission for the module
  * @return array
  *   Reurns the array of permissions introduced by the module
  */
 function linkexplorer_permission() {
 	return array(
 	  'use link explorer' => array(
 		 'title' => t('Use link explorer module'),
 		 'description' => t('Perform exlorer on nodes.'),
 	  ),
 	  'admin link explorer' => array(
 		 'title' => t('Admin link explorer settings'),
 		 'description' => t('Allows to control the access to linkexplorer settings.'),
 	  ),
   );
 }
 
 /**
  * Implementing link_explorer_form()
  * @return string|file
  *   HTML table of liks as well as link to xls file download
  */
 function link_explorer_form() {
 	$links = get_links_in_node();
 	//Code to generate excel 
 	$objPHPExcel = new PHPExcel();
 	
 	// Fill worksheet from values in array
 	$objPHPExcel->getActiveSheet()->fromArray($links, null, 'A1');
 	
 	// Rename worksheet
 	$objPHPExcel->getActiveSheet()->setTitle('Links');
 	
 	// Set AutoSize for name and email fields
 	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
 	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
 	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
 	
 	// Save Excel 2007 file
 	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
 	//$file_name = 'sites/default/files/LINKExporer.xlsx';
 	$file_name = variable_get("link_export_xls_path", 'sites/default/files');
 	$file_name .= "/LINKExporer.xlsx";
 	$objWriter->save($file_name);
 	drupal_set_message(t("Hey ! I found following links in your nodes, If it seems bigger list just click 
 	  !here and I will create excel sheet for you!", 
 	  array("!here" => l("here", url($file_name, array("absolute" => TRUE))))));
    //Create HTML table of links
 	if (!empty($links)) {
 		$limit = 50;
 		$page = pager_default_initialize(count($links), $limit, 0);
 		$offset = $limit * $page;
 		$table_rows = array_slice($links, $offset, $limit);
 	}
   
   return theme('table', array(
     'header' => array("node", "link"),
     'rows' => $table_rows,
     'attributes' => array('class' => array('mytable'))
   )) . theme("pager", array("quantity" => 50));
 }
 /**
  * Impementing extract_links()
  *   method to extract each link from multithreaded array of links
  * @param array $links_array
  *   multithreaded array of link
  * @param integer $link_node
  *   node id
  * @return array|boolean
  *  array of links or FALSE if no links are available
  */
 function extract_links($links_array = array(), $link_node=NULL) {
 	if (count($links_array) > 0 ) {
 		foreach ($links_array as $link) {
 		  if (!is_array($link)) {
 		  	//Pregmatch is not able to remove href= word from all the links so manually removing it.
 		  	$link_name = str_replace("href=\"","",$link);
 		    $links[] = array("node" => $link_node, "link" => $link_name);
 		  }
 		  else {
 		  	$links_array = extract_links($link, $link_node);
 		  	$links = array_merge($links, $links_array);
 		  }
 		}
 		if (count($links) > 0) {
 		  return $links;
 		}
 		else {
 			return FALSE;
 		}
 	}
 	else {
 		return FALSE;
 	}
 }
 /**
  * Implementing get_links_in_node()
  *   function to retrive all links from node
  * @return array $links
  *   array of links
  */
 function get_links_in_node() {
 	$query = link_explorer_load_node_contents();
 	$links = array();
 	//$regex = '/<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>/siU';
 	$regex = variable_get("link_match_regular_expression", "");
 	while ($results = $query->fetchAssoc()) {
 		$matches = NULL;
 		$string = $results['body_value'];
 		//preg_match_all('@href=[\'"]?([^\'" >]+)@', $string, $matches);
 		preg_match_all($regex, $string, $matches);
 		//drupal_set_message("<pre>". print_r($matches, TRUE) ."<hr/></pre>");
 		if (count($matches) > 0) {
 			foreach ($matches as $link) {
 				$get_links = extract_links($link, $results['entity_id']);
 				if (!empty($get_links) && is_array($get_links)) {
 					$links = array_merge($links, $get_links);
 				}
 			}
 		}	
 	}
 	return $links;
 }
 function link_explorer_remove_using_xls() {
   //Including required file
   $include_file = drupal_get_path('module', 'linkexplorer'). "/PHPExcel/IOFactory.php";
   require_once($include_file);
   //Improving cache settings.
   $cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
   $cacheSettings = array( ' memoryCacheSize ' => '50MB');
   PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
   //Read excel file
   $inputFileName = 'sites/default/files/LINKExporer.xls';
   //  Read your Excel workbook or show error if not readable
   try {
     $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
     $objReader = PHPExcel_IOFactory::createReader($inputFileType);
     $objPHPExcel = $objReader->load($inputFileName);
   } catch(Exception $e) {
     die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME));
   }
   $array = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
   $op = "";
   //If we are doing process in batch it will help us to devide process in batch
   $batch_size = variable_get("link_replace_batch_size", 0);
   $batch_start_from = variable_get("link_replace_batch_end", 1);
   $batch_end_at = ($batch_size > 0) ? $batch_start_from + $batch_size: count($array);
   //Don't process ahead if we have already proccessed all the rows.
   if ($batch_start_from >= count($array)) {
   	 return t("It seems I have already converted all the records! No further processings is required!");
   }
   for ($i=$batch_start_from; $i<=$batch_end_at; $i++) {
   	 if ($i > count($array)) {
   	 	break;
   	 }
   	 $array[$i]['C'] = trim($array[$i]['C']);
   	 if (!empty($array[$i]['C'])) {
   	   if (replace_string_and_update_node($array[$i]['A'], $array[$i]['B'], $array[$i]['C'])) {
   	   	 drupal_set_message(t("I have successfully replace  @string1 with @string2 in node @nodeid <br/>", 
   	   	   array("@string1" => $array[$i]['B'], "@string2" => $array[$i]['C'], "@nodeid" => $array[$i]['A'])));
   	   }
   	 }
   	 else {
   	   drupal_set_message(t("I don't have the replacement string for row @rownumber", 
   	     array("@rownumber" => $i)));
   	 }
   }
   //Set new start point for next process
   variable_set("link_replace_batch_end", $i);
   //Lets load all node first and then remove related contents from nodes.
   return t("I have successfully processed @numrows rows starting from @start to @end", 
     array("@numrows" => $batch_size, "@start" => $batch_start_from, "@end" => $i));
 }
 
 function link_explorer_load_node_contents ($node=NULL, $limit = FALSE, $start=0, $end=0) {
 	if (!empty($nodes)) {
 	  //Load all nodes if no specif node id is provided.
 	  $query = db_select('field_data_body', 'n')
 	    ->fields('n', array('body_value', 'entity_id'))
 	    ->condition("entity_type", "node")
 	    ->condition("entity_id", $node)
 	    ->condition('bundle', 'page');
 	}
 	else {
      $query = db_select('field_data_body', 'n')
 	    ->fields('n', array('body_value', 'entity_id'))
 	    ->condition("entity_type", "node")
 	    ->condition('bundle', 'page');
 	}
 	if ($limit == TRUE) {
 	  $query->range($start, $end);
 	}
 	return $query->execute();
 }
 function flush_batch_variables() {
 	//Delete the data where last process was end, helps to start fresh process.
 	variable_set("link_replace_batch_end", 1);
 	return t("All variables flashed successfully!");
 }
 function replace_string_and_update_node($node_id=NULL, $find = "", $replace = "") {
 	if (!empty($node_id)) {
 	  $query = link_explorer_load_node_contents($node_id);
 	  $result = $query->fetchAssoc();
 	  $body = $result['body_value'];
 	  //Move the file if finding items is file path and get filename of moved file.
 	  if (file_exists(ltrim($find, "/"))) {
 	  	$replace = linkexplorer_move_files($find, $replace);
      }
 	  $body = str_replace($find, $replace, $body);
 	  //Update the node now
 	  $query_updated = db_update('field_data_body') 
         ->fields(array(
           'body_value' => $body,
         ))
         ->condition('entity_id', $node_id, '=')
         ->execute();
 	  return TRUE;
 	}
 }
 function linkexplorer_move_files($path = NULL,$destination = NULL) {
 	return file_unmanaged_move($path, $destination, FILE_EXISTS_RENAME);
 }